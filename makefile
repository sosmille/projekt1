output: main.o class.o
	g++ main.o class.o -g -o output

main.o: main.cpp
	g++ -c -g main.cpp

class.o: class.cpp class.hpp
	g++ -c -g class.cpp

clean:
	rm *.o output
