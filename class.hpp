#ifndef class_hpp
#define class_hpp
#include <iostream>
//definicje struktur, nag��wki funkcji

class Roots{
public:
    void solution();

private:
    //coefficients
    float a,b,c;
    //results for real solution
    float x1,x2;
    //results for imaginary solution
    float Re, Im;
    float discriminant;


    };
#endif // class_hpp
