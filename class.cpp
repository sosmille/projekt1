#include "class.hpp"
#include <iostream>
#include <cmath>
#include <iomanip>
using namespace std;

void Roots::solution(){
	cout << "Enter coefficients a, b and c of your quadratic equation." << endl;
	cin >> a>> b>> c;
	discriminant = b * b - 4*a*c;

	if (discriminant > 0) {
		x1 = (-b + sqrt(discriminant)) / (2 * a);
		x2 = (-b - sqrt(discriminant)) / (2 * a);
		cout << "We have two real roots." << endl;
		cout << "x1 = " << setprecision(2)<< x1 << endl;
		cout << "x2 = " << setprecision(2)<< x2 << endl;
	}
	else if(discriminant == 0) {
		x1 = -b / (2 * a);
		cout << "We have one real root." << endl;
		cout << "x1 = x2 = " << setprecision(2)<< x1 << endl;
	}
	else {
		discriminant *= (-1);
		Re = -b / (2 * a);
		Im = sqrt(discriminant) / (2 * a);
		cout << "We have two complex roots." << endl;
		cout << "x1 = " << setprecision(2)<< Re << "+i " << setprecision(2)<< Im << endl;
		cout << "x2 = " << setprecision(2)<< Re << "-i " << setprecision(2)<< Im << endl;
	}
	//cin >> a ;
}
